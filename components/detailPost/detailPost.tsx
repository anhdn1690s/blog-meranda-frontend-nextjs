import React from 'react'
import classes from './detailPost.module.scss'
import moment from 'moment'

const DetailPost = ({ post }) => {
    const getContentFragment = (index, text, obj, type) => {
        let modifiedText = text;

        if (obj) {
            if (obj.bold) {
                modifiedText = (<b key={index}>{text}</b>);
            }

            if (obj.italic) {
                modifiedText = (<em key={index}>{text}</em>);
            }

            if (obj.underline) {
                modifiedText = (<u key={index}>{text}</u>);
            }
        }

        switch (type) {
            case 'heading-three':
                return <h3 key={index}>{modifiedText.map((item, i) => <React.Fragment key={i}>{item}</React.Fragment>)}</h3>;
            case 'paragraph':
                return <p key={index} >{modifiedText.map((item, i) => <React.Fragment key={i}>{item}</React.Fragment>)}</p>;
            case 'heading-four':
                return <h4 key={index} >{modifiedText.map((item, i) => <React.Fragment key={i}>{item}</React.Fragment>)}</h4>;
            case 'image':
                return (
                    <img
                        key={index}
                        alt={obj.title}
                        height={obj.height}
                        width={obj.width}
                        src={obj.src}
                    />
                );
            default:
                return modifiedText;
        }
    };


    return (
        <div className={classes.rootDetailPost}>
            <div className={classes.contentPost}>
                <img src={post.featuredImage.url} alt="Img" />
                <div className={classes.info}>
                    <div className={classes.user}>
                        <img src={post.author.photo.url} alt="Img" />
                        <p>{post.author.name}</p>
                    </div>
                    <div className={classes.moth}>
                        {moment(post.createdAt).format('MMM DD, YYYY')}
                    </div>
                </div>
                <h3>{post.title}</h3>
                <div className={classes.content}>
                    {post.content.raw.children.map((typeObj, index) => {
                        const children = typeObj.children.map((item, itemindex) => getContentFragment(itemindex, item.text, item));

                        return getContentFragment(index, children, typeObj, typeObj.type);
                    })}
                </div>
            </div>
        </div>
    )
}

export default DetailPost