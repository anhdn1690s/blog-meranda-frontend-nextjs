import React, { useCallback } from 'react'
import Modal from "../modal"
import Mask from "../mask"
import { motion } from 'framer-motion'
import Link from 'next/link'
import { useHeaderState } from "../../hooks/stores/useHeaderState";
import classes from './menuMobile.module.scss'

export const MenuMobile = (props) => {
    const { menuData } = props
    const isOpen = useHeaderState((state) => state.mobileLeftDrawer);
    const setMobileLeftDrawer = useHeaderState(
        (state) => state.setMobileLeftDrawer
    );
    // useLockBodyScroll(isOpen);
    const closeModal = useCallback(
        () => setMobileLeftDrawer(false),
        [setMobileLeftDrawer]
    );

    return (
        <Modal>
            <motion.div
                initial={{ translateX: '-100vw', opacity: 0 }}
                variants={{
                    open: {
                        translateX: 0,
                        opacity: 1,
                    },
                    closed: {
                        translateX: '-100vw',
                        opacity: 0,
                    },
                }}
                transition={{ ease: 'easeIn' }}
                className={classes.root}
                animate={isOpen ? 'open' : 'closed'}

            >
                <div className={classes.body}>
                    <nav className={classes.navMobile}>
                        <ul>
                            {menuData.map((category) => (
                                <li key={category.slug}>
                                    <Link href="/">
                                        {category.name}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </nav>
                    <div className={classes.close} onClick={closeModal}>X</div>
                </div>
            </motion.div>
            <Mask
                isActive={isOpen}
                classes={{ root_active: classes.maskRootActive, root: classes.maskRoot }}
                dismiss={closeModal}

            />
        </Modal>
    )
}