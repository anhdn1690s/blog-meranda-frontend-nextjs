import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import classes from './header.module.scss'
import { MenuMobile } from './menuMobile'
import { useHeaderState } from '@/hooks/stores/useHeaderState'
import { getCategories } from '@/services'

export const Header = () => {
  const setMobileLeftDrawer = useHeaderState(
    (state) => state.setMobileLeftDrawer
  );
  const [category, setCategory] = useState([]);
  useEffect(() => {
    getCategories().then((newCategory) => setCategory(newCategory))
  }, [])

  return (
    <div className={classes.rootHeader}>
      <div className={classes.logo}>
        <p>Meranda</p>
      </div>
      <div className={classes.menu}>
        <ul className={classes.nav}>
          {category.map((category) => (
            <li key={category.slug}>
              <Link href="/">{category.name}</Link>
            </li>
          ))}
        </ul>
      </div>
      <div className={classes.iconMenu} onClick={() => {
        setMobileLeftDrawer(true);
      }}>
        <svg width="22" height="17" viewBox="0 0 22 17" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M1.6665 15.1667H20.3332M1.6665 8.50001H20.3332M1.6665 1.83334H20.3332" stroke="#00171F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        </svg>
      </div>
      <MenuMobile key="mobile-menu" menuData={category} />
    </div>
  )
}
