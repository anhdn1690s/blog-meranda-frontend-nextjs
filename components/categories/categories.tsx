import { getCategories } from '@/services';
import React, { useEffect, useState } from 'react'
import classes from './categories.module.scss'

const Categories = () => {
    const [category, setCategory] = useState([]);
    useEffect(() => {
        getCategories().then((newCategory) => setCategory(newCategory))
    }, [])
    return (
        <div className={classes.rootCategories}>
            <p className={classes.title}>
                Category
            </p>
            <div className={classes.nameCategory}>
                {category.map((index) => (
                    <p key={index.slug}>{index.name}</p>
                ))}
            </div>
        </div>
    )
}

export default Categories