import React, { useEffect, useRef, useState } from 'react'
import classes from './commentsForm.module.scss'
import { submitComment } from '@/services';
const CommentsForm = ({ slug }) => {
    const [error, setError] = useState(false);
    const [localStorage, setLocalStorage] = useState(null);
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const commentEL = useRef();
    const emailEl = useRef();
    const nameEl = useRef();
    const storeDataEl = useRef();

    useEffect(() => {
        emailEl.current.value = window.localStorage.getItem('email');
        nameEl.current.value = window.localStorage.getItem('name');
    }, [])

    const handleCommentSubmission = () => {
        setError(false);

        const { value: comment } = commentEL.current;
        const { value: name } = nameEl.current;
        const { value: email } = emailEl.current;
        const { checked: storeData } = storeDataEl.current;

        if (!comment || !name || !email) {
            setError(true);
            return;
        }
        const commentObj = {
            name, email, comment, slug
        };
        if (storeData) {
            window.localStorage.setItem('name', name);
            window.localStorage.setItem('email', email);
        } else {
            window.localStorage.removeItem('name', name);
            window.localStorage.removeItem('email', email);
        }

        submitComment(commentObj).then((res) => {
            setShowSuccessMessage(true);
            setTimeout(() => {
                setShowSuccessMessage(false);
            }, 3000)
        })
    }
    return (
        <div className={classes.rootComment}>
            <h3>Comment Form</h3>
            <div className={classes.boxComment}>
                <textarea ref={commentEL} name="comment" placeholder='Comment'></textarea>
            </div>
            <div className={classes.boxNameAndEmail}>
                <input type='text' ref={nameEl} name="name" placeholder='Name'></input>
                <input type='text' ref={emailEl} name="email" placeholder='Email'></input>
            </div>
            <div className="">
                <div className="">
                    <input ref={storeDataEl} type="checkbox" id="storeData" name='storeData' value="true"></input>
                    <label>Save my e-mail and name for the next time I comment.</label>
                </div>
            </div>
            {error && <p>all fields are required</p>}
            <div className={classes.button}>
                <button type='button' onClick={handleCommentSubmission}>Add Comment</button>
                {showSuccessMessage && <p>Comment Submit</p>}
            </div>
        </div>
    )
}

export default CommentsForm