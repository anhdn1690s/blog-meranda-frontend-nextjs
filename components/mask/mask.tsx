import classNames from 'classnames'
import defaultClasses from './mask.module.scss'
interface IProps {
  dismiss: () => void
  delayedDismiss?: () => void
  isActive: boolean
  classes?: {
    [key: string]: string
  }
}

const Mask = (props: IProps) => {
  const { dismiss, isActive, classes, delayedDismiss } = props

  const className = isActive ? defaultClasses.root_active : defaultClasses.root

  return (
    <button
      className={classNames(className, classes?.['root'], {
        [classes?.['root_active']]: isActive,
      })}
      onClick={dismiss}
      onMouseOver={delayedDismiss}
    />
  )
}

export default Mask
