import React, { FunctionComponent } from 'react'
import Footer from '../footer/footer'
import { Header } from '../header/header'
import classes from './layout.module.scss'
import bgImg from '../../assets/bg_theme.png'

export const LayoutComponent = ({ children }) => {
    return (
        <div className={classes.nextRoot} style={{
            // backgroundImage: `url(${bgImg.src})`,
            // backgroundSize: 'cover',
            // backgroundPosition: 'top center',
            // backgroundRepeat: 'no-repeat',
        }}>
            <div className={classes.mainRoot}>
                <Header />
                {children}
                <Footer />
            </div>
        </div>
    )
}