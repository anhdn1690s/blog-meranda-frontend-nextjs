import React from 'react'
import classes from './postCard.module.scss'
import moment from 'moment'
import Link from 'next/link'

const PostCard = ({ post }) => {
  return (
    <div className={classes.rootPostCard}>

      <Link href={`/post/${post.node.slug}`}>
        <img src={post.node.featuredImage.url} alt="Img" />
      </Link>
      <h3><Link href={`/post/${post.node.slug}`}>{post.node.title}</Link></h3>
      <div className={classes.info}>
        <div className={classes.user}>
          <img src={post.node.author.photo.url} alt="Img" />
          <p>{post.node.author.name}</p>
        </div>
        <div className={classes.moth}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
          <p>{moment(post.node.createdAt).format('MMM DD, YYYY')}</p>
        </div>
      </div>
      <p className={classes.epx}>
        {post.node.excerpt}
      </p>
      <Link href={`/post/${post.node.slug}`}>
        <button>
          Continue Reading
        </button>
      </Link>
    </div>
  )
}

export default PostCard
