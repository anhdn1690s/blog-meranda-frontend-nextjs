import { getRecentPosts, getSimilarPosts } from '@/services';
import React, { useEffect, useState } from 'react'
import classes from './postWidget.module.scss'
import moment from 'moment'
import Link from 'next/link';

const PostWidget = ({ categories, slug }) => {
    const [relatePosts, setRelatePosts] = useState([]);

    useEffect(() => {
        if (slug) {
            getSimilarPosts(categories, slug)
                .then((result) => setRelatePosts(result))
        } else {
            getRecentPosts()
                .then((result) => setRelatePosts(result))
        }
    }, [slug])

    return (
        <div className={classes.rootPostWidget}>
            <div className={classes.mainPostWidget}>
                <p className={classes.title}>Recent Posts</p>
                {relatePosts.map((relatePost) => (
                    <Link href={`/post/${relatePost.slug}`} className={classes.post} key={relatePost.slug} >

                        <img src={relatePost.featuredImage.url} alt="Img" />
                        <div className={classes.content}>
                            <p className={classes.moth}>{moment(relatePost.createdAt).format('MMM DD, YYYY')}</p>
                            <p className={classes.titlePost}>{relatePost.title}</p>
                        </div>
                    </Link>
                ))}
            </div>
        </div >
    )
}

export default PostWidget