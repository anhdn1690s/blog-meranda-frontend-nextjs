import React from 'react'
import classes from './author.module.scss'
import Image from 'next/image';

import { grpahCMSImageLoader } from '../../util';

const Author = ({ author }) => {
    return (
        <div className={classes.rootAuthor}>
            <Image
                unoptimized
                loader={grpahCMSImageLoader}
                alt={author.name}
                height={100}
                width={100}
                src={author.photo.url}
            />
            <p className={classes.name}>
                {author.name}
            </p>
            <p className={classes.bio}>
                {author.bio}
            </p>
        </div>
    )
}

export default Author