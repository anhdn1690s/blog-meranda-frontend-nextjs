import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import PostCard from '../components/postCard/postCard'
import { getPosts } from '../services'
import classes from './home.module.scss'
import PostWidget from '@/components/postWidget/postWidget'
import Categories from '@/components/categories/categories'

export default function HomeIndex({ posts }) {
  return (
    <div className={classes.rootBody}>
      <div className={classes.leftContent}>
        {posts.map((post, index) => (
          <PostCard post={post} key={index} />
        ))}
      </div>
      <div className={classes.rightContent}>
        <div className={classes.rightRoot}>
          <PostWidget />
          <Categories />
        </div>
      </div>
    </div>
  )
}

export async function getStaticProps() {
  const posts = (await getPosts()) || [];
  return {
    props: { posts },
  };
}