import React from 'react'
import PostWidget from '@/components/postWidget/postWidget'
import Categories from '@/components/categories/categories'
import DetailPost from '@/components/detailPost/detailPost'
import { getPostDetails, getPosts } from '../../services'
import classes from '../home.module.scss'
import Author from '@/components/author/author'
import { useRouter } from 'next/router';
import CommentsForm from '@/components/commentsForm/commentsForm'
import Page404 from '../page404'
import Comments from '@/components/comments/comments'


const PostDetail = ({ post }) => {
    const router = useRouter();

    if (router.isFallback) {
        return <Page404 />;
    }

    return (
        <div className={classes.rootBody}>
            <div className={classes.leftContent}>
                <DetailPost post={post} />
                <Author author={post.author} />
                <CommentsForm slug={post.slug} />
                <Comments slug={post.slug} />
            </div>
            <div className={classes.rightContent}>
                <div className={classes.rightRoot}>
                    <PostWidget />
                    <Categories />
                </div>
            </div>
        </div>
    )
}

export default PostDetail

export async function getStaticProps({ params }) {
    const data = await getPostDetails(params.slug);
    return {
        props: {
            post: data,
        },
    };
}

// Specify dynamic routes to pre-render pages based on data.
// The HTML is generated at build time and will be reused on each request.
export async function getStaticPaths() {
    const posts = await getPosts();
    return {
        paths: posts.map(({ node: { slug } }) => ({ params: { slug } })),
        fallback: true,
    };
}