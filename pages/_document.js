import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="vi">
      <Head />
      <body>
        <Main />
        <div id="modal" />
        <NextScript />
      </body>
    </Html>
  )
}
